เหมาะสำหรับการสร้าง select option ที่มีเยอะ ๆ 100 ตัวขึ้นไป

ที่ต้อง load api มาสร้างเป็น option 

ซึ่งจะทำให้ select ตัวนั้นช้ามาก ๆ 

การแก้ไขคือสร้าง api ที่ดึงค่า Option ตามค่าที่ user พิมพ์

อย่างน้อย 2 ตัวขึ้นไป เพื่อทำให้ option ใน select มีจำนวนน้อยลง 

และไม่ทำให้ browser ช้า
```
http://localhost:8080/index-v2.html
```

## json ทั้งหมด
```
[
  {
    id: 1,
    name: "Yogesh Singh",
    username: "yssyogesh",
    password: null
  },
  {
    id: 2,
    name: "Sonarika Bhadoria",
    username: "sona",
    password: null
  },
  {
    id: 3,
    name: "Vishal Sahu",
    username: "vishal",
    password: null
  },
  {
    id: 4,
    name: "Sunil singh",
    username: "sunil",
    password: null
  },
  {
    id: 5,
    name: "Vijay mourya",
    username: "vijay",
    password: null
  },
  {
    id: 6,
    name: "Anil singh",
    username: "anil",
    password: null
  },
  {
    id: 7,
    name: "jitendra singh",
    username: "jiten",
    password: null
  }
]
```

สมมุติค่าเดิมก่อนที่จะเลือก select ตัวใหม่คือ 1

select จะโหลด name ของ id 1 เพื่อเป็นค่า เริ่มต้น

หรือค่าเดิมที่เคย save ไว้ก็ได้ ก็ทำในลักษณะเดียวกัน
```
        var id = 1;
        $.ajax({
            url: "http://localhost:8080/v2/" + id,
            type: "get",
        }).done(response => {
            var fullname = response.username + ": " + response.name;
            var option = new Option(fullname, id, true, true);
            $("#selUser").append(option).trigger('change');
        }).fail(err => {
            console.log(err);
        });
```
