package com.example.springbootforselect2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootForSelect2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootForSelect2Application.class, args);
	}

}
