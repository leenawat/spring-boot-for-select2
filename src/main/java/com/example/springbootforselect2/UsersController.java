package com.example.springbootforselect2;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsersController {

	@Autowired
	UsersRepository usersRepository;

	@GetMapping(value = "/v1")
	public List<Map<String, Object>> findByNameV1(@RequestParam String search) {
		return usersRepository.findByNameV1(search);
	}

	@GetMapping(value = "/v2")
	public List<Users> findByNameV2(@RequestParam String search) {
		return usersRepository.findByNameV2(search);
	}

	@GetMapping(value = "/v2/{id}")
	public Users findByIdV2(@PathVariable int id) {
		return usersRepository.findByIdV2(id);
	}

}
