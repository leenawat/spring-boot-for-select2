package com.example.springbootforselect2;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

@Repository
public class UsersRepository {

	@Autowired
	@Qualifier("jdbcTemplate")
	private JdbcTemplate jdbcTemplate;

	@Autowired
	@Qualifier("namedParameterJdbcTemplate")
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;

	public List<Map<String, Object>> findByNameV1(String search) {
		String sql = "select id, name as text from users where name like :search";
		SqlParameterSource namedParameters = new MapSqlParameterSource("search", "%" + search + "%");
		return namedParameterJdbcTemplate.queryForList(sql, namedParameters);
	}

	public List<Users> findByNameV2(String search) {
		String sql = "select id, name, username from users where name like :search";
		SqlParameterSource namedParameters = new MapSqlParameterSource("search", "%" + search + "%");
		return namedParameterJdbcTemplate.query(sql, namedParameters, new BeanPropertyRowMapper<Users>(Users.class));
	}

	public Users findByIdV2(int id) {
		String sql = "select id, name, username from users where id = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] { id }, new BeanPropertyRowMapper<Users>(Users.class));
	}

}
